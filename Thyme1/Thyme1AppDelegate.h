//
//  Thyme1AppDelegate.h
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Thyme1AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
