//
//  Thyme1TimerViewController.m
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import "Thyme1TimerViewController.h"
#import "Thyme1MainViewController.h"

@interface Thyme1TimerViewController ()

@end

NSArray         *dirPaths;
NSString        *docsDir;
NSMutableString *userAlertFileName;
NSString        *soundFilePath;
NSURL           *soundFileURL;
NSDictionary    *recordSettings;


@implementation Thyme1TimerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //set up the recording stuff
    //creates a different sound file for each burner
    
    userAlertFileName = [NSMutableString stringWithFormat: @"sound%d.caf", _chosenBurner];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    soundFilePath = [docsDir stringByAppendingPathComponent:userAlertFileName];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    _recordingURL = soundFileURL;
    
    //NSLog(@"chosen burner (viewDidLoad): @%d", _chosenBurner);
    NSLog (@"sound file path %@", soundFilePath);
    
    recordSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                      [NSNumber numberWithInt:AVAudioQualityMin],
                      AVEncoderAudioQualityKey,
                      [NSNumber numberWithInt:16],
                      AVEncoderBitRateKey,
                      [NSNumber numberWithInt: 2],
                      AVNumberOfChannelsKey,
                      [NSNumber numberWithFloat:44100.0],
                      AVSampleRateKey,
                      nil];
    
    NSError *error = nil;
    
    _audioRecorder = [[AVAudioRecorder alloc]
                      initWithURL:soundFileURL
                      settings:recordSettings
                      error:&error];
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
    } else {
        [_audioRecorder prepareToRecord];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)returnTimerVars:(id)sender {
    
    [self.delegate timerViewControllerDidFinish:self];
    
    //NSLog(@"returnTimerVars");
    //NSLog(@" now chosen burner %@", self.chosenBurner);
    //NSLog(@" hour label %@", self.hourLabel.text);
    //NSLog(@" hour text %@", self.hrsText);
    //NSLog(@" hour num %@", self.hrs);
    //instantiate the class and return it to MainViewController
    //Thyme1TimerViewController* myTVC = [[Thyme1TimerViewController alloc] init];
    //[self.delegate timerViewControllerDidFinish:myTVC];
    //NSLog(@"returnTimerVars before: mins--> %@", _minLabel.text);
    //NSLog(@"returnTimerVars after: secs--> %@", _secLabel.text);
    //NSLog(@"returnTimerVars after");
}


- (IBAction)hStep:(UIStepper *)sender {
    int value = [sender value];
    
    [_hourLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
}

- (IBAction)mStep:(UIStepper *)sender {
    int value = [sender value];
    
    [_minLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
}

- (IBAction)sStep:(UIStepper *)sender {
    int value = [sender value];
    
    [_secLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
}


//TODO: this should automatically disable the default alert buttons
- (IBAction)startRecording:(id)sender {
    
    //set the default alert type to 0 if recording a custom alert
    _alertType = 0;
    
    //make sure to return the default alert buttons to their default state
    [_alarmAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_beepAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_bellAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_roosterAlertButton setBackgroundColor:([UIColor whiteColor])];
    
    if (!_audioRecorder.recording)
    {
        //the recorder is recording, turn it off
        [self.recordButton setImage:([UIImage imageNamed:@"red-record-button-stop.png"]) forState:(UIControlStateNormal)];
        [_audioRecorder record];
        
    } else {
        
        //the recorder is not recording, turn it on
        [self.recordButton setImage:([UIImage imageNamed:@"red-record-button.png"]) forState:(UIControlStateNormal)];
        [_audioRecorder stop];
        
    }
}

- (IBAction)closeKeyboard:(UIButton *)sender {
    
    [self.timerName resignFirstResponder];
    //return TRUE;
    
}


//these set the alert type and highlight the selected UIbutton
- (IBAction)setAlarmAlert:(id)sender {
    
    _alertType = 1;
    [_alarmAlertButton setBackgroundColor:([UIColor lightGrayColor])];
    [_beepAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_bellAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_roosterAlertButton setBackgroundColor:([UIColor whiteColor])];
    
    NSLog(@"alarm alert");
    
}

- (IBAction)setBeepAlert:(id)sender {
    
    _alertType = 2;
    [_alarmAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_beepAlertButton setBackgroundColor:([UIColor lightGrayColor])];
    [_bellAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_roosterAlertButton setBackgroundColor:([UIColor whiteColor])];
    
    NSLog(@"beep alert");
    
}

- (IBAction)setBellAlert:(id)sender {
    
    _alertType = 3;
    [_alarmAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_beepAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_bellAlertButton setBackgroundColor:([UIColor lightGrayColor])];
    [_roosterAlertButton setBackgroundColor:([UIColor whiteColor])];
    
    NSLog(@"bell alert");
    
}

- (IBAction)setRoosterAlert:(id)sender {
    
    _alertType = 4;
    [_alarmAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_beepAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_bellAlertButton setBackgroundColor:([UIColor whiteColor])];
    [_roosterAlertButton setBackgroundColor:([UIColor lightGrayColor])];
    
    NSLog(@"rooster alert");
    
}

@end
