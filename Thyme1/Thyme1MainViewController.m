//
//  Thyme1MainViewController.m
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import "Thyme1MainViewController.h"

@interface Thyme1MainViewController ()

@end

int hours, minutes, seconds;

@implementation Thyme1MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"received info %@", self.timerPopoverController);
    
    //disable all the play and stop buttons
    _playBurnerButton1.enabled = NO;
    _stopBurnerButton1.enabled = NO;
    _playBurnerButton2.enabled = NO;
    _stopBurnerButton2.enabled = NO;
    _playBurnerButton3.enabled = NO;
    _stopBurnerButton3.enabled = NO;
    _playBurnerButton4.enabled = NO;
    _stopBurnerButton4.enabled = NO;

    //TODO: change capacity when more burners / stoves are needed?
    
    burnerTimerLabels = [[NSMutableArray alloc] init];
    [self->burnerTimerLabels addObject:(_burner1TimerLabel)];
    [self->burnerTimerLabels addObject:(_burner2TimerLabel)];
    [self->burnerTimerLabels addObject:(_burner3TimerLabel)];
    [self->burnerTimerLabels addObject:(_burner4TimerLabel)];
    
    burnerLabelLabels = [[NSMutableArray alloc] init];
    [self->burnerLabelLabels addObject:(_burner1NameLabel)];
    [self->burnerLabelLabels addObject:(_burner2NameLabel)];
    [self->burnerLabelLabels addObject:(_burner3NameLabel)];
    [self->burnerLabelLabels addObject:(_burner4NameLabel)];
    
    //NSLog(@"%@", self->burnerTimerLabels);
    /*for (int i = 1; i < 5; i++) {
     UILabel *label = [[UILabel alloc] init];
     [burnerTimerLabels addObject:label];
     }*/
    //NSLog(@"%@", burnerTimerLabels);
    //burnerTimerLabels = [[NSMutableArray alloc] init];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(Thyme1FlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
    }
}

//returning from the modal view, this is where the user should end up
- (void)timerViewControllerDidFinish:(Thyme1TimerViewController *)controller
{

    //this sets up the timer vars
    //TODO: put upper and lower limits on these numbers
    userHours   = [controller.hourLabel.text intValue];
    userMinutes = [controller.minLabel.text intValue];
    userSeconds = [controller.secLabel.text intValue];
    
    //do the calculations to properly set up the timers
    totalSeconds = userSeconds;
    totalSeconds += userMinutes * 60;
    totalSeconds += userHours * 60 * 60;
    secondsRemaining = totalSeconds;
    
    /*
     notes:
     
     now take the chosenburner / chosenstove value and assign to the proper burner / stove
     this method doesn't know which is which, so check both
     
     */
    
    //NSLog(@"first timer label object: %@", [self->burnerTimerLabels objectAtIndex:burnerObjIndex]);
    UILabel *localTimerLabel = [self->burnerTimerLabels objectAtIndex:currentBurner - 1];
    UILabel *localNameLabel = [self->burnerLabelLabels objectAtIndex:currentBurner - 1];
    
    localTimerLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", userHours, userMinutes, userSeconds];
    [localNameLabel setText:controller.timerName.text];
    
    //now eset up the state of the burner when the user returns
    _playBurnerButton1.enabled = YES;
    //_burner1.enabled = NO;
    
    //set the url of the recorded alert message (if provided)
    userAlertURL = controller.recordingURL;
    NSLog(@"controller.recordingURL: %@", controller.recordingURL);
    
    //determine if an alert has been set
    if (controller.alertType != 0){
        defaultAlertType = controller.alertType;
        NSLog(@"the alert type has been set");
    } else {
        NSLog(@"the user has chosen a custom alert, no alert type set");
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.timerPopoverController dismissPopoverAnimated:YES];
    }
    
    //NSlog();
    
    //NSMutableString *burnerUsed = [NSMutableString stringWithFormat: @"burner%dTimerLabel", currentBurner];
    // NSLog(@"burner label to update: %@", burnerUsed);
    
    //int burnerObjIndex = currentBurner - 1;
    
    //_burner1TimerLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", userHours, userMinutes, userSeconds];
    //[self.burner1NameLabel setText:controller.timerName.text];
    
    //_stopBurnerButton1.enabled = NO;//should still be NO

}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.flipsidePopoverController = nil;
}

//this is called right before the segue is executed
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
 
    NSLog(@"prepareForSegue (identifier): %@", segue.identifier);
    
    //NSLog(@" send value is: @%", sender);
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        
        //[[segue destinationViewController] setDelegate:(id<AVAudioRecorderDelegate)];
        [[segue destinationViewController] setDelegate:self];
        //[[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.flipsidePopoverController = popoverController;
            popoverController.delegate = self;
        }
    } else if ([[segue identifier] isEqualToString:@"timerSetup"]) {
        
        [_burner1TimerLabel setTextColor:([UIColor whiteColor])];
        
        
        //NSLog(@"prepareForSegue");
        
        //[[segue destinationViewController] setDelegate:self];
        //[[segue destinationViewController] setDelegate:self];
        [segue.destinationViewController setChosenBurner:currentBurner];
        [[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            
            self.timerPopoverController = popoverController;
            popoverController.delegate = self;
        }
    }
    
    //set variables here?
    
    Thyme1TimerViewController* myTVC = [[Thyme1TimerViewController alloc] init];
    
    [myTVC setChosenBurner:currentBurner];
    
    //NSLog(@" current burner is: @%d", currentBurner);
    //NSLog(@" chosen burner is: @%d", myTVC.chosenBurner);
}

- (IBAction)togglePopover:(id)sender
{
    if (self.flipsidePopoverController) {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    } else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}


- (IBAction)startTimer:(id)sender {
    
    downtimer1 = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                             target: self
                                           selector: @selector(downdateTimerLabel1)
                                           userInfo: nil
                                            repeats: YES];
    _playBurnerButton1.enabled = NO;
    _stopBurnerButton1.enabled = YES;
}

- (IBAction)stopTimer:(id)sender {

  //_playBurnerButton1.enabled = YES;
  //_stopBurnerButton1.enabled = NO;
  [downtimer1 invalidate];
    [_audioPlayer stop];
    
    //if exists
    [uptimer1 invalidate];
  
    
}

- (IBAction)isBurner1:(id)sender {
    currentBurner = 1;
    
    //Thyme1TimerViewController* myTVC = [[Thyme1TimerViewController alloc] init];
    //myTVC.chosenBurner = 1;
    
    //NSLog(@" current burner is: @%d", currentBurner);
    //NSLog(@" chosen burner is: @%d", myTVC.chosenBurner);
    
}

- (IBAction)isBurner2:(id)sender {
    currentBurner = 2;
    
    //Thyme1TimerViewController* myTVC = [[Thyme1TimerViewController alloc] init];
    //myTVC.chosenBurner = 2;

    //NSLog(@" current burner is: @%d", currentBurner);
    //NSLog(@" chosen burner is: @%d", myTVC.chosenBurner);
}

- (IBAction)isBurner3:(id)sender {
    currentBurner = 3;
    
    //Thyme1TimerViewController* myTVC = [[Thyme1TimerViewController alloc] init];
    //myTVC.chosenBurner = 3;
    //NSLog(@" current burner is: @%d", currentBurner);
    //NSLog(@" chosen burner is: @%d", myTVC.chosenBurner);
    
}

- (IBAction)isBurner4:(id)sender {
    currentBurner = 4;
    
    //Thyme1TimerViewController* myTVC = [[Thyme1TimerViewController alloc] init];
    //myTVC.chosenBurner = 4;
    //NSLog(@" current burner is: @%d", currentBurner);
    //NSLog(@" chosen burner is: @%d", myTVC.chosenBurner);
    
    
}

- (void)downdateTimerLabel1
{


    if(secondsRemaining >= 0 ){

        hours = secondsRemaining / 3600;
        minutes = (secondsRemaining % 3600) / 60;
        seconds = (secondsRemaining % 3600) % 60;
        _burner1TimerLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
        secondsRemaining -- ;

    } else if (secondsRemaining < 0) {
        
        //fire the alert or custom alert
        [self fireAlert1];
        
        //NSString* imageName = [[NSBundle mainBundle] pathForResource:@"image1" ofType:@"png"];
        //start the countup
        [downtimer1 invalidate];
        //[downtimer1];
        [self fireUpTimer1];
        
    }
    
}
- (void)fireUpTimer1
{

    //NSLog(@"count up time");
    uptimer1 = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                 target: self
                                               selector: @selector(updateTimerLabel1)
                                               userInfo: nil
                                                repeats: YES];
    _playBurnerButton1.enabled = NO;
    //_stopBurnerButton1.enabled = NO;
    [_burner1TimerLabel setTextColor:([UIColor redColor])];
    _burner1.enabled = YES;

    
    
}
- (void)fireAlert1{
    
    NSString *alertPath;
    if (defaultAlertType != 0){
        
        //alarm = 1
        //beep = 2
        //bell = 3
        //rooster = 4
        //fire whichever default alert the user has chosen
        switch (defaultAlertType) {
            case 1:
              alertPath = [[NSBundle mainBundle] pathForResource:@"Alarm_Clock" ofType:@"mp3"];
                break;
            case 2:
              alertPath = [[NSBundle mainBundle] pathForResource:@"beep-7" ofType:@"mp3"];
                break;
            case 3:
              alertPath = [[NSBundle mainBundle] pathForResource:@"bell-Fire-Alarm" ofType:@"mp3"];
                break;
            case 4:
              alertPath = [[NSBundle mainBundle] pathForResource:@"Rooster-alert-lower" ofType:@"mp3"];
                break;
            default:
                //statements
                break;
        }

        NSURL *soundFileURL = [NSURL fileURLWithPath:alertPath];
        
        NSLog(@"default alert path: %@", alertPath);
        //fire a method with the alert path, or for now, just do it here
        
        NSError *error;
        
        _audioPlayer = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:soundFileURL
                        error:&error];
        _audioPlayer.numberOfLoops = -1;
        
        //handle errors
        if (error)
            NSLog(@"Error: %@",
                  [error localizedDescription]);
        else
            [_audioPlayer play];
        
    } else {
        
        
        //fire the custom alert
        //NSURL *soundFileURL = [NSURL fileURLWithPath:userAlertURL];
        
        NSLog(@"user alert URL: %@", userAlertURL);
        
        
        //fire a method with the alert path, or for now, just do it here
        
        NSError *error;
        
        _audioPlayer = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:userAlertURL
                        error:&error];
        //_audioPlayer.numberOfLoops = -1;
        
        
        //handle errors
        if (error)
            NSLog(@"Error: %@",
                  [error localizedDescription]);
        else
            [_audioPlayer play];
        
        
        //NSLog(@" user alert URL:%@", userAlertURL);
        
    }



    
}
- (void)updateTimerLabel1
{
    countUpSeconds ++;
    //start the countup
    hours = countUpSeconds / 3600;
    minutes = (countUpSeconds % 3600) / 60;
    seconds = (countUpSeconds % 3600) % 60;
    _burner1TimerLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
    
    //count up for 5 minutes
    if (countUpSeconds == 30) {
        
        [uptimer1 invalidate];

        //put the burner back into initial shape
        _playBurnerButton1.enabled = NO;
        _stopBurnerButton1.enabled = NO;
        _burner1.enabled     = YES;
        [_burner1TimerLabel setTextColor:([UIColor whiteColor])];
        
        //reinitialize the value
        _burner1TimerLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d", userHours, userMinutes, userSeconds];
        
        _playBurnerButton1.enabled = YES;
        _burner1.enabled = NO;
    }
}


/*
 - (IBAction)playRecording:(id)sender {
 if (!_audioRecorder.recording)
 {
 //_stopButton.enabled = YES;
 _recordButton.enabled = NO;
 
 NSError *error;
 
 _audioPlayer = [[AVAudioPlayer alloc]
 initWithContentsOfURL:_audioRecorder.url
 error:&error];
 
 _audioPlayer.delegate = self;
 
 //set progress bar to 0
 //_playProgress.progress = 0.0f;//makes it a float
 
 //determine the length of the file and put the file length in the text box
 //NSTimeInterval fileDuration = _audioPlayer.duration;
 //NSString *inStr = [NSString stringWithFormat:@"%1.2f", fileDuration];
 //_updateBox.text = inStr;
 
 
 //Set timer which gets the current music time, updates UISlider in .1 sec interval
 //NSTimer *sliderTimer;
 //sliderTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
 
 //handle errors
 //if (error)
 //NSLog(@"Error: %@",
 //[error localizedDescription]);
 //else
 [_audioPlayer play];
 
 }
 
 }
 */
@end
