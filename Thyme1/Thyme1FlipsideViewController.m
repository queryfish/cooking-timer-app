//
//  Thyme1FlipsideViewController.m
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import "Thyme1FlipsideViewController.h"

@interface Thyme1FlipsideViewController ()

@end

@implementation Thyme1FlipsideViewController

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)flipStove:(id)sender {
     [self.delegate flipsideViewControllerDidFinish:self];
}
@end
