//
//  Thyme1MainViewController.h
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import "Thyme1FlipsideViewController.h"
#import "Thyme1TimerViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface Thyme1MainViewController : UIViewController <Thyme1FlipsideViewControllerDelegate, Thyme1TimerViewControllerDelegate,   UIPopoverControllerDelegate>
{
    //assign to the values returned from timer popover
    int userHours;
    int userMinutes;
    int userSeconds;
    int totalSeconds;
    int secondsRemaining;
    int countUpSeconds;
    
    NSTimer *downtimer1;
    NSTimer *uptimer1;
    
    NSURL *userAlertURL;
    
    int defaultAlertType;
    int currentBurner;
    
    NSMutableArray *burnerTimerLabels;
    NSMutableArray *burnerLabelLabels;
    
    //NSArray *dirPaths;
    //NSString *docsDir;
}

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;
@property (strong, nonatomic) UIPopoverController *timerPopoverController;

//burner buttons (opens timer setup page)
@property (weak, nonatomic) IBOutlet UIButton *burner1;
@property (weak, nonatomic) IBOutlet UIButton *burner2;
@property (weak, nonatomic) IBOutlet UIButton *burner3;
@property (weak, nonatomic) IBOutlet UIButton *burner4;

//stop / play buttons
@property (weak, nonatomic) IBOutlet UIButton *playBurnerButton1;
@property (weak, nonatomic) IBOutlet UIButton *playBurnerButton2;
@property (weak, nonatomic) IBOutlet UIButton *playBurnerButton3;
@property (weak, nonatomic) IBOutlet UIButton *playBurnerButton4;
@property (weak, nonatomic) IBOutlet UIButton *stopBurnerButton1;
@property (weak, nonatomic) IBOutlet UIButton *stopBurnerButton2;
@property (weak, nonatomic) IBOutlet UIButton *stopBurnerButton3;
@property (weak, nonatomic) IBOutlet UIButton *stopBurnerButton4;

//the timer labels
@property (weak, nonatomic) IBOutlet UILabel *burner1TimerLabel;
@property (weak, nonatomic) IBOutlet UILabel *burner2TimerLabel;
@property (weak, nonatomic) IBOutlet UILabel *burner3TimerLabel;
@property (weak, nonatomic) IBOutlet UILabel *burner4TimerLabel;

//the user label burner labels
@property (weak, nonatomic) IBOutlet UILabel *burner1NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *burner2NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *burner3NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *burner4NameLabel;

//the user label stove labels
@property (weak, nonatomic) IBOutlet UILabel *stove1Label;
@property (weak, nonatomic) IBOutlet UILabel *stove2Label;

//@property NSString* myHourLabel;


- (IBAction)startTimer:(id)sender;
- (IBAction)stopTimer:(id)sender;

- (IBAction)isBurner1:(id)sender;
- (IBAction)isBurner2:(id)sender;
- (IBAction)isBurner3:(id)sender;
- (IBAction)isBurner4:(id)sender;




@end
