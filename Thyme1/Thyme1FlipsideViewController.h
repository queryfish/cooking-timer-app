//
//  Thyme1FlipsideViewController.h
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Thyme1FlipsideViewController;

@protocol Thyme1FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(Thyme1FlipsideViewController *)controller;
@end

@interface Thyme1FlipsideViewController : UIViewController

@property (weak, nonatomic) id <Thyme1FlipsideViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *gotoStoveView;
- (IBAction)flipStove:(id)sender;

@end
