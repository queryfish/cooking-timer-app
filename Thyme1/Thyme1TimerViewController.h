//
//  Thyme1TimerViewController.h
//  Thyme1
//
//  Created by JC Jordan on 10/30/13.
//  Copyright (c) 2013 Elevate Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class Thyme1TimerViewController;

@protocol Thyme1TimerViewControllerDelegate

//this should pass back the contents of the Thyme1TimerViewController object back to main
- (void)timerViewControllerDidFinish:(Thyme1TimerViewController *)controller;

@end

//@interface Thyme1TimerViewController : UIViewController
@interface Thyme1TimerViewController : UIViewController <AVAudioRecorderDelegate>

@property (weak, nonatomic) id <Thyme1TimerViewControllerDelegate> delegate;

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
//@property (strong, nonatomic) AVAudioPlayer   *audioPlayer;


//close the popover
@property (weak, nonatomic) IBOutlet UIButton *returnTimerVarsButton;



@property (weak, nonatomic) IBOutlet UIButton *recordButton;

@property (weak, nonatomic) IBOutlet UIStepper *hourStepper;
@property (weak, nonatomic) IBOutlet UIStepper *minStepper;
@property (weak, nonatomic) IBOutlet UIStepper *secStepper;

@property (weak, nonatomic) IBOutlet UILabel *hourLabel;
@property (weak, nonatomic) IBOutlet UILabel *minLabel;
@property (weak, nonatomic) IBOutlet UILabel *secLabel;

@property (weak, nonatomic) IBOutlet UITextField *timerName;

@property NSString*  hrsText;
@property NSInteger* hrs;
@property NSInteger* mins;
@property NSInteger* secs;

@property int  alertType;

@property NSURL*     recordingURL;

@property int chosenBurner;
@property int chosenStove;

//set up the alert
@property (weak, nonatomic) IBOutlet UIButton *alarmAlertButton;
@property (weak, nonatomic) IBOutlet UIButton *beepAlertButton;
@property (weak, nonatomic) IBOutlet UIButton *bellAlertButton;
@property (weak, nonatomic) IBOutlet UIButton *roosterAlertButton;


@property (weak, nonatomic) IBOutlet UIButton *recButton;

//@property (nonatomic) BOOL startStopRecButtonIsActive;


- (IBAction)returnTimerVars:(id)sender;

- (IBAction)hStep:(UIStepper *)sender;
- (IBAction)mStep:(UIStepper *)sender;
- (IBAction)sStep:(UIStepper *)sender;

- (IBAction)startRecording:(id)sender;


- (IBAction)closeKeyboard:(UIButton *)sender;


- (IBAction)setAlarmAlert:(id)sender;
- (IBAction)setBeepAlert:(id)sender;
- (IBAction)setBellAlert:(id)sender;
- (IBAction)setRoosterAlert:(id)sender;


- (void)setChosenBurner:(int)chosenBurner;


@end;
